from django.contrib import admin

# Register your models here.
from users.models import User


# Register your models here.


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id','first_name', 'last_name', 'email', 'username', 'is_active')
    search_fields = ('id', 'first_name', 'email', 'username', 'is_active')